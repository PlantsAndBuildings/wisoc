import json
import csv
import sys

json_obj = json.load(open(sys.argv[1],'r'))
to_be_itered = json_obj['comments']

fp = open(sys.argv[1]+'.csv','wb')
writer = csv.writer(fp)

for comment in to_be_itered:	
	writer.writerow([comment['root'].encode('utf-8')])

