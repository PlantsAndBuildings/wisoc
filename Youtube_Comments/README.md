**HOW TO EASILY GET ALL THE COMMENTS ON A YOUTUBE VIDEO**

1. First run this:  
$ scraper \<url of video\> > some\_output\_file  
DO NOT specify an extension to som\_output\_file

2. Now run this:  
$ python json\_to\_csv.py some\_output\_file  
If all of this runs successfully then you will end up with some\_output\_file.csv that contains the required comments.  

Cheers
