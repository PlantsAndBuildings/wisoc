import theano, theano.tensor
import numpy 
from theano import shared, function
import random
import sys, os

#theano.config.compute_test_value = 'warn'
#theano.config_exception_verbosity = 'high'
theano.config.fast_compile = 'off'


langs = ['bn','gu','hi','kn','ml','mr','ta','te']
fp_langs = []

'''
VARIABLES FOR L-CLASSIFIER
'''
CHAR_SET = [chr(ord('a')+i) for i in range(26)]
N_GRAMS_LIMIT = 2
CHAR_SET_SZ = 26
TRAINING_SET_FACTOR = 3
N_IN = 26**2 + 26
N_OUT = len(langs)
LEARNING_RATE = 0.01
NUM_EPOCHS = 10

def clean(word_list):
	clean_word_list = []
	for word in word_list:
		if all(char.isalpha() for char in word):
			clean_word_list.append(word)
	return clean_word_list

def get_feature_vector(word):
	feature_vector = []
	global CHAR_SET
	for char1 in CHAR_SET:
		if char1 in word:
			feature_vector.append(1.0)
		else:
			feature_vector.append(0.0)
	for char1 in CHAR_SET:
		for char2 in CHAR_SET:
			if char1+char2 in word:
				feature_vector.append(1.0)
			else:
				feature_vector.append(0.0)
	return numpy.array(feature_vector,dtype=numpy.float32)


def remove_en(word_list):
	pass	

class EN_Classifier:
	def __init__(self,positive_examples_filename, negative_examples_filename):
		
		self.n_grams_limit = 2
		self.learning_rate = 0.5
		self.char_set = [chr(ord('a')+i) for i in range(26)]
		self.char_set_sz = len(self.char_set)
		self.n_in = 26**2 +26
		self.n_out = 1
		self.num_epochs = 5

		self.pos_examples = self.get_clean_word_list(positive_examples_filename)
		self.neg_examples = self.get_clean_word_list(negative_examples_filename)
		
		
		self.W_val_init = numpy.array([[random.randint(1,1000)/1000.0 for i in range(self.n_out)] for j in range(self.n_in)])
		self.W = theano.shared(name='W',value=self.W_val_init)
		self.b_val_init = numpy.array([random.randint(1,1000)/1000.0 for i in range(self.n_out)])
		self.b = theano.shared(name='b',value=self.b_val_init)
		
		self.x = theano.tensor.dvector(name='x')
		#self.x.tag.test_value = get_feature_vector('world')
		#print(self.x.tag.test_value)
		#raw_input()
		self.pre_activation = (theano.tensor.dot(self.x,self.W)+self.b)/10.0
		#print(self.pre_activation.tag.test_value)
		#raw_input()
		self.y = theano.tensor.nnet.sigmoid(self.pre_activation)
		#print(self.y.tag.test_value)
		#raw_input()

		self.label = theano.tensor.ivector(name='label')
		#self.label.tag.test_value = [1.0]
		#self.cost = theano.tensor.nnet.categorical_crossentropy(self.y,self.label)
		self.cost = -theano.tensor.dot(self.label,theano.tensor.log(self.y))-theano.tensor.dot((1-self.label[0]),theano.tensor.log(1 - self.y[0]))
		#print(self.cost.tag.test_value)

		self.dcost_dW, self.dcost_db = theano.grad(cost=self.cost,wrt=[self.W,self.b])

		self.W_upd = (self.W,self.W-self.learning_rate*self.dcost_dW)
		self.b_upd = (self.b,self.b-self.learning_rate*self.dcost_db)
		
		self.train = theano.function(inputs=[self.x,self.label],outputs=[self.y, self.cost, self.pre_activation, self.dcost_dW, self.dcost_db], updates=[self.W_upd,self.b_upd])

		self.test = theano.function(inputs=[self.x],outputs=[self.y,self.pre_activation])

		self.train_en_classifier()
	
	def get_clean_word_list(self, filename):
		fp = open(filename,'r')
		return clean(fp.read().lower().split())

	def train_en_classifier(self):
		training_list = zip(self.pos_examples,[1.0]*len(self.pos_examples))
		training_list.extend(zip(self.neg_examples,[0.0]*len(self.neg_examples)))
		random.shuffle(training_list)
		neg_cnt = 0
		pos_cnt = 0
		cnt = 0
		print(len(training_list))
		raw_input()
		for example in training_list:
			if example[1] == 1:
				pos_cnt += 1
			elif example[1] == 0:
				neg_cnt += 1 
			cnt += 1
			self.train(get_feature_vector(example[0]),[example[1]])
			if cnt%800000 == 0:
				self.save_weights_and_biases('weights_'+str(cnt),'biases_'+str(cnt))
		print('Training Complete! Balle Balle!')
		print "Positive Examples ",pos_cnt
		print "Negative examples ",neg_cnt
		self.save_weights_and_biases('weights_'+str(cnt),'biases_'+str(cnt))
	
	def save_weights_and_biases(self,weights_filename,biases_filename):
		weights = open(weights_filename,'w')
		for line in self.W.get_value():
			weights.write(str(line))
			print line
		biases = open(biases_filename,'w')
		biases.write(str(self.b.get_value()))
		print self.b.get_value()
		

	def is_en(self,word):
		return self.test(get_feature_vector(word))[0]
	'''	
	def load_weights_and_biases(self,weights_filename, biases_filename):
		wf = open(weights_filename,'r')
		weights = re.split(r''wf.read())
		bf = open(biases_filename,'r')
	'''
#train_input_file = open('train_data/Input1.txt','r')
#train_annotation_file = open('train_data/Annotation1.txt','r')
#test_input_file = open('test_data/Input1.txt','r')
#test_annotation_file = open('test_data/Annotation1.txt','r')


en_classifier = EN_Classifier('train_data/en_positive_examples','train_data/en_negative_examples')

#en_classifier.load_weights_and_biases('weights_2400000','biases_2400000')

while True:
	word = raw_input()
	print(en_classifier.is_en(word))

'''
for lang in langs:
	print lang
	fp = open('train_data/'+lang+'.csv','r')
	fp_langs.append(fp)


lang_word_lists = []
for fp in fp_langs:
	clean_list = clean(fp.read().lower().split())
	en_removed_clean_list = remove_en(clean_list)
	lang_word_lists.append(clean_list)

W_val = numpy.array([[random.randint(1,1000)/1000.0 for i in range(N_OUT)] for j in range(N_IN)])
W = theano.shared(name='W',value=W_val)
b_val = numpy.array([random.randint(1,1000)/1000.0 for i in range(N_OUT)])
b = theano.shared(name='b',value=b_val)

x = theano.tensor.dvector(name='x')
pre_activation = theano.tensor.dot(x,W)+b
y = theano.tensor.nnet.softmax(pre_activation)[0]

label = theano.tensor.ivector(name='label')
cost = theano.tensor.nnet.categorical_crossentropy(y,label)

dcost_dW, dcost_db = theano.grad(cost=cost,wrt=[W,b])

W_upd = (W,W-LEARNING_RATE*dcost_dW)
b_upd = (b,b-LEARNING_RATE*dcost_db)

train = theano.function(inputs=[x,label],outputs=[y, cost, pre_activation, dcost_dW, dcost_db], updates=[W_upd,b_upd])

test = theano.function(inputs=[x],outputs=[y,pre_activation])

sum_words = 0
for word_list in lang_word_lists:
	sum_words += len(word_list)
	print(word_list)
	raw_input()
print(sum_words)

raw_input()
for epoch in range(NUM_EPOCHS):
	for lang in langs:
		training_list = []
		num_words_of_lang = len(lang_word_lists[langs.index(lang)])
		for word in lang_word_lists[langs.index(lang)]:
			training_list.append((word,lang))
		for i in range(TRAINING_SET_FACTOR):
			for j in range(num_words_of_lang):
				random_lang = random.sample(langs,1)[0]
				randomly_chosen_word = random.sample(lang_word_lists[langs.index(random_lang)],1)[0]
				training_list.append((randomly_chosen_word,random_lang))
		random.shuffle(training_list)
		for example in training_list:
			feature_vector = get_feature_vector(example[0])
			label_vector = [0 for i in range(N_OUT)]
			label_vector[langs.index(example[1])] += 1
			label_vector = numpy.array(label_vector,dtype=numpy.int32)
			train(feature_vector,label_vector)
			#raw_input()

print 'Training Complete! Balle Balle!'

while True:
	inp = raw_input()
	inp_feature_vector = get_feature_vector(inp)
	print(numpy.argmax(test(inp_feature_vector)[0]))


'''
