from HMM_NER import InputParser 

xml_file = open('dummy_xml_output','w')
sentences_file = open('dummy_sentences_file','r')
ip = InputParser(sentences_file = 'dummy_sentences_file',nerc_annotations_file = 'dummy_nerc_annotations_file')
all_annotations = ip.annotations

for sentence,annotations in zip(sentences_file,all_annotations):
	xml = sentence
	for annotation in annotations:
		xml = ('<'+annotation[1][1:-1]+'>'+annotation[0]+'</'+annotation[1][1:-1]+'>').join(xml.split(annotation[0]))
	xml_file.write(xml)


