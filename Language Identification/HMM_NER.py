import os,sys,random,numpy


# The HMM will have the following states -- in the future this can be taken as input from user

# NE 	-- Named Entity
# NNE 	-- Not a Named Entity
# SS 	-- Start of Sentence
# ES 	-- End of Sentence

STATES = ['NE','NNE','SS','ES','UNK']
VOCABULARY = {}

class InputParser:
	def __init__(self, **kwargs):
		if 'sentences_file' not in kwargs.keys():
			raise InputParserException('sentences_file not specified')
		if 'nerc_annotations_file' not in kwargs.keys():
			raise InputParserException('nerc_annotations_file not specified')
		self.valid_annotations = [
			'(person)',
			'(organization)',
			'(location)',
			'(date)',
			'(number)',
			'(percentage)',
			'(money)',
			]
		self.no_named_entities_string = 'no_named_entities_here'
		self.sentences = []
		self.annotations = []
		with open(kwargs['sentences_file'],'rb') as sentences_file:
			while True:
				sentence = sentences_file.readline()
				if sentence == '': break
				print(sentence)
				# raw_input()
				self.sentences.append(sentence.strip())
		with open(kwargs['nerc_annotations_file']) as nerc_annotations_file:
			while True:
				annotations = nerc_annotations_file.readline()
				if annotations == '': break
				print(annotations)
				# raw_input()
				self.annotations.append(self.string_to_annotations(annotations))
		self.data = zip(self.sentences, self.annotations)

			
	
	
	def string_to_annotations(self, annotation_string):
		# an annotation is represented as a tuple
		# Each annotation tuple contains the word in the first position and annotation in the second position
		# For example, ('daivik','person') or ('Tata Institute of Fundamental Research','organization')
		annotations = []
		if annotation_string == self.no_named_entities_string: return annotations
		for raw_annotation in annotation_string.split(','):
			raw_annotation = raw_annotation.strip()
			valid_annotation_cnt = 0
			for valid_annotation in self.valid_annotations:
				if valid_annotation in raw_annotation:
					valid_annotation_cnt += 1
					annotations.append(((''.join(raw_annotation.split(valid_annotation))).strip(),valid_annotation))
			if valid_annotation_cnt < 1:
				pass
			# TODO: This fails in cases like 'daivik (person) (person)'
			elif valid_annotation_cnt > 1:
				raise InvalidAnnotationException('\n\tIn \"'+annotation_string+'\"\n\tCheck \"'+raw_annotation+'\".\n\tThere seems to be more than one valid annotations')
		return annotations

class InputParserException(Exception):
	pass

class InvalidAnnotationException(Exception):
	pass
		

# Constructor takes 3 arguments:
# 1. states -- define the states as a list of strings
# 2. observations -- define the observations as a list of strings
# 3. training_input -- define the training input as InputParser.data list (list of tuples)

class HMM:
	def __init__(self,**kwargs):
		global STATES
		if 'training_data' not in kwargs.keys():
			raise HMMException('training_data not specified')
		self.training_data = kwargs['training_data']
		self.states, self.num_states = STATES, len(STATES)
		self.transition_matrix = self.init_transition_matrix(num_states = self.num_states)
		self.observation_matrix = self.init_observation_matrix(num_observations = self.num_observations,num_states = self.num_states)
		self.priors = self.calculate_priors()
		
		# Various training methods
		# 1. Baum-Welch Training
		self.baum_welch_training()
		# 2. Naive training using word counts
		self.naive_training()


	def init_observation_matrix(self,**kwargs):	
		if 'num_states' not in kwargs.keys():
			raise HMMException('num_states not defined')
		if 'num_observations' not in kwargs.keys():
			raise HMMException('num_observations not defined')
		num_states = kwargs['num_states']
		num_observations = kwargs['num_observations']
		obs_mat = numpy.random.dirichlet(numpy.ones(num_states),size=num_observations)
		return obs_mat


	def init_transition_matrix(self,**kwargs):
		if 'num_states' not in kwargs.keys():
			raise HMMException('num_states not defined')
		num_states = kwargs['num_states']
		trans_mat = numpy.random.dirichlet(numpy.ones(num_states),size=num_states) 
		return trans_mat

	def __str__(self):
		ret = 'Number of states: '+str(self.num_states)+'\n'
		ret += 'States: '+str(self.states)+'\n'
		ret += 'Number of observations: '+str(self.num_observations)+'\n'
		ret += 'Observations : '+str(self.observations)+'\n'
		ret += 'Transition Matrix:\n'
		ret += str(self.transition_matrix)+'\n'
		ret += 'Observation Matrix:\n'
		ret += str(self.observation_matrix)
		return ret

	# TODO: Complete these functions
	def baum_welch_training(self):
		pass

	def calculate_priors(self, **kwargs):
		pass
	
	def naive_training(self):
		pass
	
	def test(self,**kwargs):
		pass


class HMMException(Exception):
	pass

# x = HMM(observations=[],states=[])
#ifp = InputParser(sentences_file='dummy_sentences_file.csv',nerc_annotations_file='dummy_nerc_annotations_file.csv')
#x = HMM(states=['Named Entity','Not a Named Entity'],observations=['Deep','Daivik','juice'])
#print(x)

#x = InputParser()
#print x.string_to_annotations('daivik (person), tata institute of fundamental research (organization)')


# Note to self --  read from plaintext file rather than .csv file. Good presentation is not worth the headache of import csv.

if __name__ == '__main__':
	training_data = InputParser(sentences_file='dummy_sentences_file',nerc_annotations_file='dummy_nerc_annotations_file')
	lolHMM = HMM(training_data=training_data.data,states=training_data.valid_annotations,observations=['Deep','Daivik','juice'])
	print lolHMM
	print lolHMM.training_data


