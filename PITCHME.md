#HSLIDE

### LANGUAGE IDENTIFICATION

#### Problem Introduction

* In the general Language Identification (LI) problem, given some text we have to determine what natural language the given content is in. 
* In code-mixed sentences, the task is to do the same for each word (that is, word-level language identification). 
* The presence of more than one language in a code-mixed sentence makes language identification a tough task. 

#HSLIDE

### Problem Introduction (contd.)

* A large volume of text found on online social media such as Facebook and Twitter contains code-mixed data that is Romanized (or transliterated to Roman script) which poses the following challenges:
	1. Tone is informal and words are abbreviated and misspelt quite often.
	2. For a given non-English word, there may be several possible transliterations (Romanizations).

#HSLIDE

For example:

* "that's good pm sahab ne apne ko defense karne k liye 1857 ko choose kiya but don't forget 1857 ki ladai k leader was a Muslim named bhahdur shah zafar , so I wanna say pm shahb hum Muslims aap ki kitni help karte hain , so plz hume apna hi samjhiye"

#HSLIDE

### Problem Formulation: 
Given a sentence in Roman Scipt ```s = <w(1), w(2), w(3), ... , w(n)>``` we have to label each word, ```w(i)```, as one of:

|			|			|
|-----------------------|-----------------------|
| en : English		| mr : Marathi		|
| bn : Bengali		| ta : Tamil		|
| gu : Gujarati		| te : Telugu		|
| hi : Hindi		| NE : Named Entity	|
| kn : Kannada		| X : None of these	|

#HSLIDE

### Current Approach

* Clean the data
	* Remove letters that occur more than twice consecutively. Example: "soooo","loool"
	* Remove words that do not belong to any language. Example: "haha","wow", onomatopoeic words like "bzzz" etc.
	* (Still under consideration): Apply heuristics to correct spelling errors to avoid multiple entries of the same word in the vocabulary.

#VSLIDE

* Identify and remove all Named Entities.
* (Still under consideration): Identify all English words first.
* Identify (distinguish) between words of different languages.

#HSLIDE

### Named Entity Recognition (NER)

* Presently, I am trying to build a Hidden Markov Model (HMM) to identify named entities.
* Use of HMMs for NER is not a new concept (Identifinder by Bikel et al) (HMM based chunk tagger).
* But these have all been tested on monolingual datasets like MUC-6

#VSLIDE

### Approaches to NER

* Dictionary based/Gazetteer list approach
* HMM based approach (Identifinder by Bikel et al, Zhou and Su at ACL 2002)
* Conditional Random Fields based approach (ISM Dhanbad in FIRE 2014 -- they used Stanfords NE tagger, DAIICT in FIRE 2014 NER task)
* SVM based approach (DAIICT in FIRE 2014 NER task)
* Show FIRE 2015 Overview Paper

_Note:_ These techniques are also used in the next step of Language Identification.

#HSLIDE

* Show diagram for Bikels HMM and proposed changes.
* Discuss Training Methods
	* Naive Training based on word counts
	* Baum-Welch Training
	* Viterbi Training (look into this later)

#HSLIDE

* For Language Identification, I plan to use Logistic Regression/Simple feedforward MLP.
* Basic Implementation in log_reg.py
