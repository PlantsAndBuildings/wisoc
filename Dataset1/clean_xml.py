import re
import sys

ip = open(sys.argv[1],'r')
op = open(sys.argv[1]+'_clean','w')

for line in ip:
	clean = re.sub('<[^<]+>','',line)
	clean = re.sub('\r','',clean)
	clean = re.sub('[\s]*\n','',clean)
	clean = re.sub('\t','',clean)
	if clean!= '': op.write(clean+'\n')


