from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time
import sys
driver = webdriver.Firefox()
driver.get("https://www.google.com/inputtools/try/")
lang_select = driver.find_element_by_id(":fs")
lang_select.click()
# Afrikaans - :cg
'''Dictionary for choice of language'''
choice = {}
choice['gu'] = ':dg'
choice['hi'] = ':dk'
choice['kn'] = ':du'
choice['ml'] = ':e9'
choice['mr'] = ':ed'
choice['ta'] = ':fc'
choice['te'] = ':fe'
choice['bn'] = ':cp'
choice['en'] = ':d6'
choice['NEL'] = ':d6'
choice['MIX'] = ':d6'
choice['NA_L'] = ':d6'
choice['Pn'] = ':d6'
choice['NE_ta'] = ':d6'
choice['nl'] = ':d6'
choice['pu'] = ':d6'
choice['NE'] = ':d6'
choice['NE_kn'] = ':d6'
choice['O'] = ':d6'
choice['U'] = ':d6'
choice['X'] = ':d6'
choice['ta.'] = ':d6'
choice['knmake'] = ':d6'
choice['OThi'] = ':d6'
choice['XKa'] = ':d6'
choice['enkadhallle'] = ':d6'
lang = driver.find_element_by_id(choice[sys.argv[1]])
lang.click()

text_area = driver.find_element_by_id("demobox")
text_area.click()

time.sleep(1.5)
#First argumrnt is input file and second is outputfile
input_file = open("/home/daivik/wisoc/TokenFiles/InputTokens/Token_File_"+sys.argv[1],"r")
output_file = open("/home/daivik/wison/TokenFiles/OutputTokens/Token_File_"+sys.argv[1],"w+")	


for line in input_file:
	words = line.split()
	for word in words:
		for character in word:
			text_area.send_keys(character)
			time.sleep(0.1)
		time.sleep(1.0)
		text_area.send_keys(Keys.RETURN)
		time.sleep(1.0)
	#print(text_area.get_attribute("value"))
all_words = text_area.get_attribute("value").encode("utf8").split()


for word in all_words:
	output_file.write(word.strip()+'\n')


print('File Token_File_'+sys.argv[1]+' done!')
